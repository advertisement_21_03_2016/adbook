<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 4/17/2016
 * Time: 9:07 PM
 */

namespace frontend\controllers;


use frontend\models\Advertisement;
use yii\helpers\Json;
use yii\web\Controller;

class AdvertisementApiController extends Controller
{
    public function actionList(){
        $result['advertisement'] = Advertisement::find()->asArray()->all();
        if(!$result){
            return Json::encode(['code'=>0, 'message' =>'Not advertisement to display', 'data' => null]);
        }
        return Json::encode(['code' =>1,'message'=>'success', 'data' => $result]);
    }
}