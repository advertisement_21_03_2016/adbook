<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $categoryname
 * @property string $url_address
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryname'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['categoryname', 'url_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryname' => 'Categoryname',
            'url_address' => 'Url Address',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
