<?php
/**
 * Created by Tuan.
 * User: PC
 * Date: 4/17/2016
 * Time: 7:35 PM
 */

namespace backend\controllers;


use Yii;
use yii\web\Controller;
use backend\models\Category;
use yii\helpers\Json;

class CategoryApiController extends Controller
{
    /**
     * @return string
     * "code":1,
    "message":"success",
    "data":{
    "category":[
    {
    "id":"3",
    "categoryname":"xe co",
    "url_address":"fed",
    "created_at":"2016-04-18 15:10:27",
    "updated_at":"2016-04-18 15:10:27"
    },
    {
    "id":"4",
    "categoryname":"dien tu",
    "url_address":"fdsfvds",
    "created_at":"2016-04-18 15:10:34",
    "updated_at":"2016-04-18 15:10:34"
    }
    ]
    }
    }
     */
    public function actionList(){
        $result['category'] = Category::find()->asArray()->all();
        if(!$result){
            return Json::encode(['code' => 0, 'message' => 'Not category to display', 'data' => null]);
        }
        return Json::encode(['code' => 1,'message' => 'success', 'data' => $result]);
    }
}